package DAO;

import DTO.TollEvent;
import DAOExceptions.DAOExceptions;

import java.util.ArrayList;
import java.util.HashSet;

public interface ITollEventDAOInterface {
    public void writeValidEventsToDataBase(HashSet<TollEvent> set)throws DAOExceptions;
    public String readAndPrintAllTollEvents()throws DAOExceptions;
}
