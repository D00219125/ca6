package DAO;

import DAOExceptions.DAOExceptions;
import DTO.TollEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

public class MySQLTollEventDAO extends MySQLDAO implements ITollEventDAOInterface{
    /**
     * Writes valid toll events to the database
     * @param tollEventHashSet HashSet containing valid TollEvents
     * @throws DAOExceptions
     */
    public void writeValidEventsToDataBase(HashSet<TollEvent> tollEventHashSet) throws DAOExceptions {
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = this.getConnection();
            String query = "insert into tollevent(TollBooth, Registration, ImageId, Timestamp) VALUES (?, ?, ?, ?)";
            ps = con.prepareStatement(query);
            for(TollEvent event : tollEventHashSet)
            {
                ps.setString(1,event.getTollBoothId());
                ps.setString(2, event.getVehicleReg());
                String s = Long.toString(event.getImageID());
                ps.setString(3, s);
                ps.setString(4,event.getTimeStamp());
                System.out.println(ps.toString());
                ps.addBatch();

            }
            ps.executeBatch();
            System.out.println("BatchJob: Tolls added to Database");
        } catch (SQLException e) {
            throw new DAOExceptions("batchJob SQL Exception: " + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOExceptions("findAllUsers() " + e.getMessage());
            }
        }
    }

    /**
     * Reads all database entries and puts them into a String
     * @return String of all the database entries
     * @throws DAOExceptions
     */
    @Override
    public String readAndPrintAllTollEvents() throws DAOExceptions {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder s = new StringBuilder();
        try
        {
            con = this.getConnection();
            String query = "select * from tollevent";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next())
            {
                String tb = rs.getString("TollBooth");
                String reg = rs.getString("Registration");
                String id = rs.getString("ImageId");
                long imgId = Long.valueOf(id).longValue();
                String ts = rs.getString("Timestamp");
                TollEvent t = new TollEvent(tb,reg,imgId,ts);
                s.append(t.toString());
                s.append("\t");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return s.toString();
    }


}
