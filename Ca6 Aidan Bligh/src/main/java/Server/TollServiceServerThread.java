package Server;

import Core.TollServiceDetails;
import DAO.ITollEventDAOInterface;
import DAO.MySQLTollEventDAO;
import DAOExceptions.DAOExceptions;
import DTO.TollEvent;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class TollServiceServerThread extends Thread{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    private static HashSet<TollEvent> validTollEvents = new HashSet<>();
    private static ArrayList<TollEvent>invalidTollEvents = new ArrayList<>();

    /**
     * Constructor for Threaded Toll Server
     * @param group
     * @param name
     * @param dataSocket
     * @param number
     */
    public TollServiceServerThread(ThreadGroup group, String name, Socket dataSocket, int number)
    {
        super(group, name);
        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream());
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    /**
     * I thought I'd need or's or ands in the if statements which is why there isn't a switch in this method
     */
    @Override
    public void run()
    {
        String incomingMessage = "";
        String response;

        try
        {
            while(!incomingMessage.equals(TollServiceDetails.END_SESSION))
            {
                response = null;

                //Take input from the client
                incomingMessage = input.nextLine();
                System.out.println("Received message: " + incomingMessage);

                //Break the input up into components
                String[] components = incomingMessage.split(TollServiceDetails.COMMAND_SEPARATOR);
                ITollEventDAOInterface tollEventDAOInterface = new MySQLTollEventDAO();
                //Process the information supplied by the client
                if(components[0].equals(TollServiceDetails.HEARTBEAT))
                {
                    response = TollServiceDetails.BEATING;
                    output.println(response);
                }
                else if(components[0].equals(TollServiceDetails.END_SESSION))
                {
                    response = TollServiceDetails.SESSION_TERMINATED;
                    output.println(response);
                }
                else if(components[0].equals(TollServiceDetails.GET_REGISTERED_VEHICLES))
                {
                    response = TollServiceDetails.RETURN_REGISTERED_VEHICLES + TollServiceDetails.COMMAND_SEPARATOR + TxtToJSONFormat();
                    output.println(response);
                }
                else if(components[0].equals(TollServiceDetails.PUT_VEHICLES_IN_DATABASE))
                {
                    response = TollServiceDetails.HASHSET_CONFIRMED + TollServiceDetails.COMMAND_SEPARATOR +TollServiceDetails.ARRAYLIST_CONFIRMED;
                    output.println(response);
                    getListOfTollEvents(components[1]);
                    getListOfTollEvents(components[2]);
                    try
                    {
                        tollEventDAOInterface.writeValidEventsToDataBase(validTollEvents);
                        response = TollServiceDetails.WRITE_TO_DATABASE_SUCCESS;
                    }
                    catch(DAOExceptions e)
                    {
                        System.out.println("you messed something up aidan");
                        response = TollServiceDetails.WRITE_TO_DATABASE_FAIL;
                    }

                    output.println(response);
                }
                else if(components[0].equals(TollServiceDetails.SHOW_VEHICLES_IN_DATABASE))
                {
                    response = TollServiceDetails.RETURN_VEHICLES_IN_DATABASE + TollServiceDetails.COMMAND_SEPARATOR + tollEventDAOInterface.readAndPrintAllTollEvents();
                    output.println(response);
                }
                else
                {
                    response = TollServiceDetails.UNRECOGNISED;
                    output.println(response);
                }
                output.flush();
            }
        }
        catch(NoSuchElementException | DAOExceptions nse)
        {
            System.out.println(nse.getMessage());
        }
        finally
        {
            try
            {
                System.out.println("\n Closing connection with client #" + number + "...");
                dataSocket.close();
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect: " + ioe.getMessage());
                System.exit(1);
            }
        }
    }

    /**
     * I couldn't get jackson working so I parsed it manually
     * @return A vaguely json like String
     */
    private String TxtToJSONFormat()
    {
        String space = "";
        String seperator = ",";
        String txtToJsonString = "";
        try
        {
            BufferedReader br = new BufferedReader(new FileReader("TollEventTest.txt"));
            while((space = br.readLine()) != null)
            {
                String[] input = space.split(seperator);
                txtToJsonString += "{"+"\"TollBoothId\":" +input[0] + "\","
                        + "\"Registration\":" + input[1] + "\","
                        + "\"ImageId\":"+input[2] + "\","
                        + "\"TimeStamp\":"+input[3] + "\""
                        +"}" + ",";
            }
            return txtToJsonString.substring(0,txtToJsonString.length()-1);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return txtToJsonString;
    }

    /**
     * This is essentially a copy of the getListOfTollEvents from the client except it parses my TollEvent toStrings(instead of json) into  TollEvents
     * @param response ToStrings of toll events
     */
    private static void getListOfTollEvents(String response)
    {
        String afterTheSeperator = response.substring(response.indexOf(TollServiceDetails.COMMAND_SEPARATOR)+2,response.length());
        System.out.println("this is seperated"+afterTheSeperator);
        //this is where I'd unparse the json if I could get that working
        String tb,reg,timestamp;
        long id;
        boolean hasNextEvent = true;
        while(hasNextEvent)
        {
            String event = response.substring(response.indexOf("{"), response.indexOf("}"));
            System.out.println(event);
            int startIndex = event.indexOf("Id=")+3;
            int endIndex = event.indexOf("veh");
            tb = event.substring(startIndex, endIndex);
            System.out.println(tb);
            startIndex = event.indexOf("eg=")+3;
            endIndex = event.indexOf("ima");
            reg = event.substring(startIndex, endIndex);
            System.out.println(reg);
            startIndex = event.indexOf("ID=")+3;
            endIndex = event.indexOf(".jpg");
            try {
                id = Long.parseUnsignedLong(event.substring(startIndex, endIndex));
            }
            catch (NumberFormatException e) {
                System.out.println("No Negative Image Id's it's invalid.");
                id=-1;
            }
            System.out.println(id);
            startIndex = event.indexOf("mp=")+3;
            endIndex = event.length();//I need this for the if at the end
            timestamp = event.substring(startIndex, endIndex);
            System.out.println(timestamp);
            TollEvent t = new TollEvent(tb,reg,id,timestamp);
            if(t.isValid())
            {
                validTollEvents.add(t);
                System.out.println(t + " Added to hashset");
            }
            else
            {
                invalidTollEvents.add(t);
                System.out.println(t + " Added to arraylist");
            }
            if(!response.substring(endIndex).contains("{ Tol"))
            {
                hasNextEvent = false;
            }
        }
    }
}
