package Server;

import Core.TollServiceDetails;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TollServiceServer {
    /**
     * Main method for the server side, a new thread is made per client
     * @param args
     */
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        ThreadGroup group = null;
        try
        {
            listeningSocket = new ServerSocket(TollServiceDetails.LISTENING_PORT);
            group = new ThreadGroup("Client threads");
            group.setMaxPriority(Thread.currentThread().getPriority() -1);
            boolean continueRunning = true;
            int threadCount = 0;
            while(continueRunning)
            {
                dataSocket = listeningSocket.accept();

                threadCount++;
                System.out.println("The server has taken " + threadCount + " commands.");

                TollServiceServerThread newCommand = new TollServiceServerThread(group, dataSocket.getInetAddress()+"",dataSocket,threadCount);
                newCommand.start();

            }
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                //Will deal with dataSocket in the thread class
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println(ioe.getMessage());
                System.exit(1);
            }
        }
    }

}
