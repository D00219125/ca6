package DTO;

import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TollEvent {
    private String vehicleReg;
    //use .intvalue() to return as an int
    private long imageID = 0;
    private String timeStamp;
    private boolean valid;

    String tollBoothId;

    /**
     * Constructor for TollEvent from old project, I didn't need to use it in this project but I changed some of it before I realized that
     * @param vehicleReg Vehicles registration is entered, the rest is automatically generated
     */
    public TollEvent(String vehicleReg) {
        this.vehicleReg = vehicleReg;
        this.imageID = ++imageID;
        this.timeStamp = "08/05/2020";
        this.valid = isImageIdValid(imageID);
        this.tollBoothId = "TB_M50";
    }

    /**
     * Constructor for TollEvent used in this project, It may be improved by removing the long for the image ID
     * @param tollBoothId ID of the toolbooth
     * @param vehicleReg Registration of the vehicle
     * @param imageID Image number
     * @param timeStamp Time of toll event
     */
    public TollEvent(String tollBoothId, String vehicleReg, long imageID, String timeStamp) {
        this.tollBoothId = tollBoothId;
        this.vehicleReg = vehicleReg;
        this.imageID = imageID;
        this.timeStamp = timeStamp;
        this.valid = isImageIdValid(imageID);
    }

    /**
     * This is what I used to make toll events valid or invalid as the registrations caused me too many issues last project
     * @param id
     * @return
     */
    private boolean isImageIdValid(long id)
    {
        return id > 0;
    }

    public String getVehicleReg() {
        return vehicleReg;
    }

    public long getImageID() {
        return imageID;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public boolean isValid() {
        return valid;
    }

    public String getTollBoothId() {
        return tollBoothId;
    }

    /**
     * ToString for TollEvent used for printing and parsing
     * @return String of toll event
     */
    @Override
    public String toString() {
        return "TollEvent{" +
                " TollBoothId="+ tollBoothId +
                " vehicleReg=" + vehicleReg +
                " imageID=" + imageID + ".jpg"+
                " timeStamp=" + timeStamp +
                '}';
    }
}
