package Client;

import Core.TollServiceDetails;
import DTO.TollEvent;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

public class TollServiceClient {
    public static HashSet<TollEvent> tollEvents = new HashSet<>();
    public static ArrayList<TollEvent> invalidTollEvents = new ArrayList<>();
    public static void main(String[] args) {
        Socket dataSocket = null;
        try
        {
            //client connection
            dataSocket = new Socket("localhost", TollServiceDetails.LISTENING_PORT);
            //client output
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out), true);
            //client input
            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));
            //user input
            Scanner kb = new Scanner(System.in);
            String message = "";
            //String to store json response
            String JSONResponse = "";
            while(!message.equals(TollServiceDetails.END_SESSION))
            {
                displayMenu();
                int choice = getNumber(kb);
                String response = "";
                if(choice >= 0 && choice <= 5)
                {
                    switch(choice)
                    {
                        //ends session
                        case 0:
                            message = TollServiceDetails.END_SESSION;
                            output.println(message);
                            output.flush();
                            if(response.equals(TollServiceDetails.SESSION_TERMINATED))
                            {
                                System.out.println("Session Ended");
                            }
                            break;
                        //verifies connection
                        case 1:
                            message = TollServiceDetails.HEARTBEAT;
                            output.println(message);
                            output.flush();
                            response = input.nextLine();
                            System.out.println("Received response: " + response);
                            break;
                        //receives list of vehicles from servers txt file
                        case 2:
                            message = TollServiceDetails.GET_REGISTERED_VEHICLES;
                            output.println(message);
                            response = input.nextLine();
                            JSONResponse = response;
                            System.out.println("Recieved the following toll events:\n"+response);
                            break;
                        //converts the vehicle list from case 2 to toll events and puts them in a HashSet or ArrayList
                        case 3:
                            //was going to use.isBlank but the version language messed up
                            if(JSONResponse.isEmpty())
                            {
                                System.out.println("You haven't downloaded any toll events to process.");
                            }
                            else
                                {
                                    getListOfTollEvents(JSONResponse);
                                }
                            break;
                        //Sends the HashSet and ArrayList to the server, responds with a message saying if they were written to the database or not
                        case 4:
                            if(invalidTollEvents.isEmpty())
                            {
                                System.out.println("Arraylist with invalid toll events is empty");
                            }
                            if(tollEvents.isEmpty())
                            {
                                System.out.println("Hashset with valid toll events is empty, will not upload nothing to the server. put something in");
                            }
                            else
                                {
                                    message = TollServiceDetails.PUT_VEHICLES_IN_DATABASE + TollServiceDetails.COMMAND_SEPARATOR
                                            + tollEvents + TollServiceDetails.COMMAND_SEPARATOR + invalidTollEvents;
                                    output.println(message);

                                    //get response saying ready
                                    response = input.nextLine();
                                    response.split(TollServiceDetails.COMMAND_SEPARATOR);
                                    System.out.println(response);
                                    //these are doubled
                                    response = input.nextLine();
                                    System.out.println(response);
                                }
                            break;
                        //Prints toll events currently in database
                        case 5:
                            message = TollServiceDetails.SHOW_VEHICLES_IN_DATABASE;
                            output.println(message);

                            response = input.nextLine();
                            String[] components = response.split(TollServiceDetails.COMMAND_SEPARATOR);
                            System.out.println(components[0]);
                            System.out.println(components[1]);
                            System.out.println(components.length);
                    }
                }
                else
                {
                    System.out.println("Please select a number from 0 to 5 from the menu");
                }
            }
            System.out.println("Thank you for using the Combo Service system");

        }
        catch (UnknownHostException UHE)
        {
            System.out.println(UHE.getMessage());
        }
        catch (IOException IOE)
        {
            System.out.println(IOE.getMessage());
        }
    }

    /**
     * Method that gracefully responds to input mismatch exceptions
     * @param kb
     * @return An integer
     */
    private static int getNumber(Scanner kb) {
        boolean numEntered = false;
        int num = -1;
        System.out.println("Enter a number.");
        while(!numEntered)
        {
            try
            {
                num = kb.nextInt();
                numEntered= true;
            }
            catch(InputMismatchException IME)
            {
                System.out.println("Try to enter a number properly this time");
                kb.nextLine();
            }
        }
        kb.nextLine();
        return num;
    }

    /**
     * Displays menu with all of the functions I have made available
     */
    private static void displayMenu() {
        System.out.println("Enter 0 to quit");
        System.out.println("Enter 1 to get Heartbeat");
        System.out.println("Enter 2 to Get All Valid Vehicles");
        System.out.println("Enter 3 to Process a toll event");
        System.out.println("Enter 4 to send toll events to database");
        System.out.println("Enter 5 to print all the toll events currently in the database");
    }

    /**
     * This is my method to parse the json formatted string into TollEvents and add them to a HashSet or ArrayList, depending on if they're valid or not.
     * @param response
     */
    private static void getListOfTollEvents(String response)
    {
        String afterTheSeparator = response.substring(response.indexOf(TollServiceDetails.COMMAND_SEPARATOR)+2,response.length());
        System.out.println("this is separated"+afterTheSeparator);
        //this is where I'd unparse the json if I could get that working
        String tb,reg,timestamp;
        long id;
        String [] singleTollEvents = afterTheSeparator.split("},");
        for(int i = 0; i<singleTollEvents.length;i++)
        {
            //I know this is the worst possible way to do things but I'm at risk of short circuiting my brain and rebooting into spanish mode
            System.out.println(singleTollEvents[i]);
            int splitIndex = singleTollEvents[i].indexOf("hId")+5;
            int commaIndex = singleTollEvents[i].indexOf(TollServiceDetails.JSON_SEPERATOR);
            tb = singleTollEvents[i].substring(splitIndex,commaIndex);
            //System.out.println(tb);
            singleTollEvents[i] = singleTollEvents[i].substring(commaIndex+2);
            splitIndex = singleTollEvents[i].indexOf("ion")+5;
            commaIndex = singleTollEvents[i].indexOf(TollServiceDetails.JSON_SEPERATOR);
            reg = singleTollEvents[i].substring(splitIndex,commaIndex);
            //System.out.println(reg);
            singleTollEvents[i] = singleTollEvents[i].substring(commaIndex+2);
            //System.out.println(singleTollEvents[i]);
            splitIndex = singleTollEvents[i].indexOf("eId")+5;
            commaIndex = singleTollEvents[i].indexOf(TollServiceDetails.JSON_SEPERATOR);
            try {
                id = Long.parseUnsignedLong(singleTollEvents[i].substring(splitIndex,commaIndex));
            }
            catch (NumberFormatException e) {
                System.out.println("No Negative Image Id's it's invalid.");
                id=-1;
            }
            //System.out.println(id);
            singleTollEvents[i] = singleTollEvents[i].substring(commaIndex+2);
            //System.out.println(singleTollEvents[i]);
            splitIndex = singleTollEvents[i].indexOf("amp")+5;
            timestamp = singleTollEvents[i].substring(splitIndex,singleTollEvents[i].length()-2);
            //System.out.println(timestamp);
            TollEvent t = new TollEvent(tb,reg,id,timestamp);

            if(t.isValid())
            {
                tollEvents.add(t);
                System.out.println(t + " Added to hashset");
            }
           else
               {
                   invalidTollEvents.add(t);
                   System.out.println(t + " Added to arraylist");
               }
        }
    }
}
