package Core;


public class TollServiceDetails {
    public static final int LISTENING_PORT = 50001;

    //command seperator
    public static final String COMMAND_SEPARATOR = "%%";
    public static final String JSON_SEPERATOR = "\",";

    //command Strings
    public static final String END_SESSION = "QUIT";
    public static final String HEARTBEAT = "Heartbeat.";
    public static final String GET_REGISTERED_VEHICLES = "Get Registered Vehicles";
    public static final String PUT_VEHICLES_IN_DATABASE = "Put Vehicles in Database";

    public static final String SHOW_VEHICLES_IN_DATABASE = "Show vehicles in database";

    //response strings
    public static final String RETURN_REGISTERED_VEHICLES = "ReturnRegisteredVehicles";
    public static final String BEATING = "BEATING";
    public static final String UNRECOGNISED = "UNKNOWN_COMMAND";
    public static final String SESSION_TERMINATED = "Thank you for using this sub par toll system.";
    public static final String RETURN_VEHICLES_IN_DATABASE = "Here are the vehicles from database";
    public static final String HASHSET_CONFIRMED = "Hashset sent...";
    public static final String ARRAYLIST_CONFIRMED = "Arraylist sent...";
    public static final String WRITE_TO_DATABASE_SUCCESS = "hashset successfully written to database.";
    public static final String WRITE_TO_DATABASE_FAIL = "hashset unsuccessfully written to database. :(";


}
